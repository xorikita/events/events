from django.contrib import admin
from .models import *

admin.site.register(Customer)
admin.site.register(Curator)
admin.site.register(Type)
admin.site.register(Location)
admin.site.register(Event)
admin.site.register(Order)