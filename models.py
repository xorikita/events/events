from django.db import models
from django.db.models import Avg, Count, Min, Sum

# Create your models here.

class Customer(models.Model):
    login = models.CharField(max_length=100, verbose_name="Имя")
    password = models.CharField(max_length=300, verbose_name="Пароль")
    def __str__(self):
        return self.login
    class Meta:
        verbose_name = 'Пользователи'
        verbose_name_plural = 'Пользователь'

class Curator(models.Model):
    name = models.CharField(max_length=100, verbose_name="Имя")
    phone = models.CharField(max_length=20, verbose_name="Телефон")
    def __str__(self):
        return self.name
    class Meta:
        verbose_name = 'Кураторы'
        verbose_name_plural = 'Куратор'

class Type(models.Model):
    name = models.CharField(max_length=100, verbose_name="Название")
    price = models.DecimalField(default=0, max_digits=12, decimal_places=2, verbose_name="Цена")
    def __str__(self):
        return self.name
    class Meta:
        verbose_name = 'Типы'
        verbose_name_plural = 'Тип'

class Location(models.Model):
    name = models.CharField(max_length=100, verbose_name="Название")
    adress = models.CharField(max_length=300, verbose_name="Адрес")
    price = models.DecimalField(default=0, max_digits=12, decimal_places=2, verbose_name="Цена")
    def __str__(self):
        return self.name
    class Meta:
        verbose_name = 'Места Проведения'
        verbose_name_plural = 'Место Проведения'

class Event(models.Model):
    name = models.CharField(max_length=100, verbose_name="Название")
    description = models.TextField(max_length=1000,blank=True, null=True, verbose_name="Описание")
    image = models.ImageField(upload_to='event', verbose_name="Фото")
    type_id = models.ForeignKey(Type, on_delete=models.CASCADE, verbose_name="Тип", related_name='event_ids',)
    location_id = models.ForeignKey(Location, on_delete=models.CASCADE, verbose_name="локация", related_name='event_ids',)
    price = models.DecimalField(default=0, max_digits=12, decimal_places=2, verbose_name="Цена", null=True, blank=True, editable=False)

    def save(self, *args, **kwargs):
        self.price = self.type_id.price + self.location_id.price
        super(Event, self).save(*args, **kwargs)
    def __str__(self):
        return self.name
    class Meta:
        verbose_name = 'Мероприятия'
        verbose_name_plural = 'Мероприятие'

class Order(models.Model):
    customer_id = models.ForeignKey(Customer, on_delete= models.CASCADE, verbose_name="Заказчик", related_name='order_ids',)
    curator_id = models.ForeignKey(Curator, on_delete= models.SET_NULL,blank=True, null=True, verbose_name="Куратор", related_name='order_ids',)
    event_id = models.ForeignKey(Event, on_delete= models.SET_NULL,blank=True, null=True, verbose_name="Мероприятие",)
    date = models.CharField(max_length=300, verbose_name="Дата")
    price = models.DecimalField(default=0, max_digits=12, decimal_places=2, verbose_name="Цена", null=True, blank=True, editable=False)

    def save(self, *args, **kwargs):
        self.price = self.event_id.price
        super(Order, self).save(*args, **kwargs)
    def __str__(self):
        return self.customer_id.login + ': ' + self.event_id.name
    class Meta:
        verbose_name = 'Заказы'
        verbose_name_plural = 'Заказ'