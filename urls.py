from django.urls import path
from .views import *

urlpatterns = [
    path('register/', RegisterView.as_view(), name='register'),
    path('get_user/', GetCustomer.as_view(), name='get_customer'),
    path('login/', LoginCustomer.as_view(), name='login'),
    path('get_data/', GetData.as_view(), name='get_data'),
    path('create_order/', CreateOrder.as_view(), name='create_order'),
    path('delete_order/', DeleteOrder.as_view(), name='delete_order'),
    path('get_orders/', getOrders.as_view(), name='get_orders'),
]