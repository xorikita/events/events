from django.shortcuts import render
from rest_framework.views import APIView
from django.db.models import Avg, Count, Min, Sum
from .models import *
from .serializer import *
from rest_framework.response import Response
from rest_framework import serializers
from django.apps import apps
import time, datetime
# Create your views here.

class RegisterView(APIView):
    def post(self, request):
        try:
            user = Customer.objects.get(login=request.data['login'])
        except Customer.DoesNotExist:
            user = None
        if user:
            return Response({'message': 'Логин уже занят'})
        serializer = CustomerSerializer(data=request.data)
        if serializer.is_valid(raise_exception=True):
            user = serializer.save()
            return Response({'id': user.id, 'login': user.login,})
        return Response({'message': 'Что то пошло не так'})

class GetCustomer(APIView):
    def post(self, request):
        user = Customer.objects.get(pk=request.data['id'])
        output ={
                    'id': user.id,
                    'login': user.login,
                } if user else {"message": "this id dosent exist"}
        return Response(output)

class LoginCustomer(APIView):
    def post(self, request):
        try:
            user = Customer.objects.get(login=request.data['login'], password=request.data['password'])
        except User.DoesNotExist:
            user = None
        output ={
                    'id': user.id,
                    'login': user.login,
                } if user else {"message": "Пользователь не найден."}
        return Response(output)


class GetData(APIView):
    def post(self, request):
        nameModel = request.data.get('model')
        filters = request.data.get('filters')
        record_id = request.data.get('id')
        if(nameModel):
            gettedModel = apps.get_model(app_label='events', model_name=nameModel)
            class DynamicSerializer(serializers.ModelSerializer):
                class Meta:
                    model = gettedModel
                    fields = '__all__'
            if(record_id):
                objects = gettedModel.objects.get(id=int(record_id))
                output = DynamicSerializer(objects).data
                return Response(output)
            elif(filters):
                queries = {
                    key: int(filters[key])
                    for key in filters.keys()
                }
                objects = gettedModel.objects.filter(**queries)
            else:
                objects = gettedModel.objects.all()
            output = DynamicSerializer(objects, many=True).data
            return Response(output)
        else:
            return Response({"message": "this model dosent exist"})

class CreateOrder(APIView):
    def post(self, request):
        serializer = OrderSerializer(data=request.data)
        if serializer.is_valid(raise_exception=True):
            order = serializer.save()
            return Response({'id': order.id, 'message': 'заказ успешно создан',})
        return Response({'message': 'Что то пошло не так'})

class DeleteOrder(APIView):
    def post(self, request):
        record_id = request.data.get('id')
        if (record_id):
            order = Order.objects.get(id=int(record_id))
            order.delete()
            return Response({'id': order.id, 'message': 'заказ успешно удалён',})
        return Response({'message': 'Что то пошло не так'})

class getOrders(APIView):
    def post(self, request):
        customer_id = request.data.get('id')
        if(customer_id):
            customer = Customer.objects.get(id=int(customer_id))
            output = [
                {
                    'id': order.id,
                    'name': order.event_id.name,
                    'image': order.event_id.image.url,
                    'price': order.price,
                    'date': order.date,
                }for order in customer.order_ids.all()
            ]
            return Response(output)
        return Response({'message': customer_id})